import requests, os, json
import argparse

# Get our environment variables
from dotenv import load_dotenv
load_dotenv()

# Get our issue we want to check
parser = argparse.ArgumentParser()
parser.add_argument('project_id', help='The project to check')
parser.add_argument('issue_id', help='The issue to check')
parser.add_argument('--quiet', help='Suppress all output', action='store_true')
args = parser.parse_args()

issue_id = int(args.issue_id)
project_id = int(args.project_id)

# Set up API parameters
GITLAB_API_BASE='https://gitlab.com/api/v4'
GITLAB_API_KEY = os.getenv('GITLAB_API_KEY', '')
AUTH_HEADERS = {
    'Authorization':f'Bearer {GITLAB_API_KEY}'
}

# Record a list of all nodes we have visited

def visit_node(project_id, issue_id, visited=[]):
    '''
    Examine the specified issue to see if we have visited it before
    and also traverse any of its links that are blockers

    Keyed off the 'blocks' status of the node, rather than 'relates_to'
    or 'is_blocked_by'

    Returns True if we have visited this node before
    '''
    
    visited = visited[:] # We need a copy of the visited list, not a reference

    node = (project_id, issue_id)
    print(f'[*] Visiting {node}') if not args.quiet else None

    if node in visited:
        return True
    else:
        visited.append((project_id, issue_id))

    url=f'{GITLAB_API_BASE}/projects/{project_id}/issues/{issue_id}'
    response = requests.get(
        url,
        headers=AUTH_HEADERS
    )
    body = json.loads(response.text)

    # Does  this issue have blocking issues?
    if 'blocking_issues_count' in body and int(body['blocking_issues_count']) > 0:
        url=f'{GITLAB_API_BASE}/projects/{project_id}/issues/{issue_id}/links'

        response = requests.get(
            url,
            headers=AUTH_HEADERS
        )
        body = json.loads(response.text)
        
        # Exclude all links that aren't blockers
        body = [x for x in body if x['link_type']=='blocks']

        # For each link, iterate through to record that issue and traverse it's children
        for link in body:
            if visit_node(link['project_id'], link['iid'], visited=visited):
                print(f"[!] Seen issue {link['iid']} already!")
                return True

    return False

results = visit_node(project_id, issue_id)
print(f'Circular dependency? {results}') if not args.quiet else None

import sys
sys.exit(results)
