import requests, os, json
import argparse
import subprocess

# Get our environment variables
from dotenv import load_dotenv
load_dotenv()

# Get our issue we want to check
parser = argparse.ArgumentParser()
parser.add_argument('project_id', help='The project to check')
args = parser.parse_args()

project_id = int(args.project_id)

# Set up API parameters
GITLAB_API_BASE='https://gitlab.com/api/v4'
GITLAB_API_KEY = os.getenv('GITLAB_API_KEY', '')
AUTH_HEADERS = {
    'Authorization':f'Bearer {GITLAB_API_KEY}'
}

# Retrieve a list of all issues in a project

def retrieve_issue_page(page=1):

    # Get a page of issues
    url=f'{GITLAB_API_BASE}/projects/{project_id}/issues?page={page}'
    response = requests.get(
        url,
        headers=AUTH_HEADERS
    )
    body = json.loads(response.text)

    print(f'[*] Iterating page {page} of results') # Can't get total pages due to API limits

    # Iterate over each issue returned on this page
    for issue in body:
        result = subprocess.run(['python', 'issue_checker.py',  str(project_id), str(issue['iid']), '--quiet'])
        if result.returncode != 0:
            print(f'[*] Circular depdency for issue: {issue["iid"]}')

    # If there is no next page, we're done
    if 'X-Next-Page' not in response.headers or response.headers['X-Next-Page'] == '':
        return None

    # Return the next page to traverse
    next_page = response.headers['X-Next-Page']
    return next_page


next_page = 1
while next_page:
    next_page = retrieve_issue_page(next_page)
