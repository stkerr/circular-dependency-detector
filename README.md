# circular-dependency-detector

# Overview 
This project detects circular blocking dependencies between GitLab issues.
It can analyze a whole project or an individual issue.

## Whole projects

To use:

```
python project_iterator.py <project id> 
```

## Individual Issues

To use:

```
python issue_checker.py <project id> <issue id> [--quiet]
```

## Dependencies

You need to install the dependencies in the `requirements.txt` file. You can
do this with `pip install -r requirements.txt`.

## Authentication

You can use this script on public issues directly. If you want to look at
private issues or if public issues depend on private issues, you will need
to give the script a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

Do this by creating a file called `.env` in the same directory as `main.py`.
The contents of this file should look like:

```
GITLAB_API_KEY=<your token>
```
